import random as r
import os

n = 8
pocet_min = 8
#funkce tisknuti pole
def hraci_pole():
    global hodnota_min
    print()
    print("\t\t\t   MINY\n")
    znak = "   "
    for i in range(n):
        znak = znak + "     " + str(i + 1)
    print(znak)   
 
    for j in range(n):
        znak = "     "
        if j == 0:
            for k in range(n):
                znak = znak + "______" 
            print(znak + '_')
 
        znak = "     "
        for k in range(n):
            znak = znak + "|     "
        print(znak + "|")
         
        znak = "  " + str(j + 1) + "  "
        for k in range(n):
            znak = znak + "|  " + str(hodnota_min[j][k]) + "  "
        print(znak + "|") 
 
        znak = "     "
        for k in range(n):
            znak = znak + "|_____"
        print(znak + '|')
    
    print()

#funkce pro rozmisteni min
def dat_miny():

    global cisla

    pocet = 0
    while pocet < pocet_min:

        pozice = r.randint(0, n*n-1)
        j = pozice // n
        k = pozice % n

        if cisla[j][k] != -1:
            pocet += 1
            cisla[j][k] = -1
#funkce nastaveni hodnot ostatnich poli
def nast_hodnoty():

    global cisla
    
    for j in range(n):
        for k in range(n):
        #kontrola zda policko ma minu
            if cisla[j][k] == -1:
                continue
        #kontrola okolnich policek zda obsahuji miny
            if j > 0 and cisla[j-1][k] == -1:
                cisla[j][k] += 1
            if j < n-1 and cisla[j+1][k] == -1:
                cisla[j][k] += 1
            if k > 0 and cisla[j][k-1] == -1:
                cisla[j][k] += 1
            if k < n-1 and cisla[j][k+1] == -1:
                cisla[j][k] += 1
            if j > 0 and k > 0 and cisla[j-1][k-1] == -1:
                cisla[j][k] += 1
            if j > 0 and k < n-1 and cisla[j-1][k+1] == -1:
                cisla[j][k] += 1
            if j < n-1 and k > 0 and cisla[j+1][k-1] == -1:
                cisla[j][k] += 1
            if j < n-1 and k < n-1 and cisla[j+1][k+1] == -1:
                cisla[j][k] += 1
#funkce zobrazujici okolni prazdna pole
def sousedici(j,k):
    global hodnota_min
    global klik
    global cisla

    #zda bylo policko vybrano
    if [j,k] not in klik:
        klik.append([j,k])
    
    if cisla[j][k] == 0:
        hodnota_min[j][k] = 0

        if j > 0:
            sousedici(j-1, k)
        if j < n-1:
            sousedici(j+1, k)
        if k > 0:
            sousedici(j, k-1)
        if k < n-1:
            sousedici(j, k+1)
        if j > 0 and k > 0:
            sousedici(j-1, k-1)
        if j > 0 and k < n-1:
            sousedici(j-1, k+1)
        if j < n-1 and k > 0:
            sousedici(j+1, k-1)
        if j < n-1 and k < n-1:
            sousedici(j+1, k+1)
    
    if cisla[j][k] != 0:
        hodnota_min[j][k] = cisla[j][k]

def instrukce():
    print("Instrukce:\n1. Zadejte číslo řádku a slopce s mezerou mezi nimi - \"1 2\"\n2. Pro označení políčka vlajkou přidejte písmeno f nebo F na konec, ukázka:\"1 2 F\"" )
def konec_hry():
    global hodnota_min
    #pocet nenulovych hodnot v polich
    pocet = 0

    for j in range(n):
        for k in range(n):

            if hodnota_min[j][k] != ' ' and hodnota_min[j][k] != 'F':
                pocet += 1
    #kontrola shody
    if pocet == n^2 - pocet_min:
        return True
    else:
        return False

#zobrazeni vsech min na konci
def zobraz_miny():
    global hodnota_min
    global cisla

    for j in range(n):
        for k in range(n):
            if cisla[j][k] == -1:
                hodnota_min[j][k] = 'X'
#cyklus hry
if __name__ == "__main__":
    #Hodnota pole
    cisla = [[0 for l in range(n)] for m in range(n)]
    #zobrazene hodnoty
    hodnota_min = [[' ' for l in range(n)] for m in range(n)]
    #pozice vlajek
    vlajky = []

    dat_miny()
    nast_hodnoty()
    instrukce()
    konec = False
    while not konec:
        hraci_pole()
        vstup = input("Zadejte číslo řádku mezeru a poté číslo sloupce = ").split()
        #vstup hledaci
        if len(vstup) == 2:
            #chybny vstup
            try:
                hodnota = list(map(int, vstup))
            except ValueError:
                print("Špatný vstup!")
                instrukce()
                continue
        #vstup oznaceni
        elif len(vstup) == 3:
            if vstup[2] != 'F' and vstup[2] != 'f':
                print("Špatný vstup!")
                instrukce()
                continue
            #chybny vstup
            try:
                hodnota = list(map(int, vstup))
            except ValueError:
                print("Špatný vstup!")
                instrukce()
                continue
            #kontrola ze vstup je v poli
            if hodnota[0] > n or hodnota[0] < 1 or hodnota[1] > n or hodnota[1] < 1:
                print("Špatný vstup!")
                instrukce()
                continue
            #zjistit kde bylo vybrano
            j = hodnota[0] - 1
            k = hodnota[1] - 1
            #pokud je v poli jiz vlajka
            if [j,k] in vlajky:
                print("Již označeno vlajkou")
                continue
            #pokud bylo pole jiz odhaleno
            if hodnota_min[j][k] != ' ':
                print("Hodnota již zobrazena")
                continue
            if len(vlajky) < pocet_min:
                print("Označeno vlajkou")
                vlajky.append([j,k])
                hodnota_min[j][k] = 'F'
                continue
            else:
                print("Položeny všechny vlajky")
                continue
        else:
            print("Špatný vstup!")
            instrukce()
            continue
        if hodnota[0] > n or hodnota[0] < 1 or hodnota[1] > n or hodnota[1] < 1:
                print("Špatný vstup!")
                instrukce()
                continue
        j = hodnota[0] - 1
        k = hodnota[1] - 1
        #odebrani vlajky
        if [j,k] in vlajky:
            vlajky.remove([j,k])
        #trefa miny
        if cisla[j][k] == -1:
            hodnota_min[j][k] = 'X'
            zobraz_miny()
            hraci_pole()
            print("Našli jste minu. Konec hry!")
            konec = True
            continue
        #odhaleni okolnich policek
        elif cisla[j][k] == 0:
            klik = []
            hodnota_min[j][k] = '0'
            sousedici(j,k)
        #pokud je v okoli mina
        else:
            hodnota_min[j][k] = cisla[j][k]
        #kontrola konce hry
        if (konec_hry()):
            zobraz_miny()
            hraci_pole()
            print("Dobrá práce! Vyhráli jste!")
            konec = True
            continue
